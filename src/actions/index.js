import {API_KEY, URL_BASE_FORECAST} from '../components/constants/api_url';
import TransformForecast from '../components/services/TransformForecast';

export const SET_CITY = 'SET_CITY'
export const SET_FORECAST_DATA = 'SET_FORECAST_DATA'

const setCity = payload => ({type: SET_CITY, payload})
const setForecastData = payload => ({ type: SET_FORECAST_DATA, payload})

export const setSelectedCity = payload => {
    return dispatch => {
        const url = `${URL_BASE_FORECAST}?q=${payload}&appid=${API_KEY}&units=metric`;
        // Activar en el estado un indicador de busqueda de datos
        dispatch(setCity(payload))
        //
        return fetch(url).then( 
            data => data.json())
        .then( weather_data => {
            dispatch(setForecastData({
                city: payload, 
                forecastData: TransformForecast(weather_data)
            }))
        })
    }
}