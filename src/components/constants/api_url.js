export const API_KEY = '';
export const URL_BASE_WEATHER = 'http://api.openweathermap.org/data/2.5/weather';
export const URL_BASE_FORECAST = 'http://api.openweathermap.org/data/2.5/forecast';