import {
    CLOUD,
    SUNNY,
    RAIN,
    SNOW,
    THUNDER,
    DRIZZLE
}   from '../constants/weathers';

const getWeatherState  = weather => {
    const { id } = weather;
    if(id < 300){
        return THUNDER
    } else if( id < 400) {
        return DRIZZLE;
    } else if( id < 600) {
        return RAIN;
    } else if( id < 700) {
        return SNOW;
    } else if(id === 800) {
        return SUNNY;
    } else {
        return CLOUD;
    }
}

const TransformWeather = weather_data => {
    const { humidity, temp } = weather_data.main;
    const { speed } = weather_data.wind;
    const watherState = getWeatherState(weather_data.weather[0]);
    const data = {
        temperature: temp,
        weatherState: watherState,
        humidity: humidity,
        wind: `${speed} m/s`,
    }
    return data
}

export default TransformWeather;