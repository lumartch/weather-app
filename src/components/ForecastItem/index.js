import React, {Component} from 'react';
import PropTypes from 'prop-types';
import WeatherData from '../../components/WeatherLocations/WeatherData/';

export class ForecastItem extends Component{
    render(){
        const {weekDay, hour, data} = this.props
        return(
            <div>
                <div>
                    <h3>{weekDay} Hora: {hour}</h3>
                </div>
                <WeatherData
                data={data}></WeatherData>
            </div>
        )
    }
}

ForecastItem.propTypes = {
    weekDay: PropTypes.string.isRequired,
    hour: PropTypes.number.isRequired,
}

export default ForecastItem;