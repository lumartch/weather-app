import React, { Component } from 'react';
import WeatherLocation from './WeatherLocations';
import PropTypes from 'prop-types';

import './styles.css';

export class LocationList extends Component{

    _handleWeatherLocationClick = (city, onSelectedLocation) => {
        onSelectedLocation(city)
    }   

    render(){
        const {cities, onSelectedLocation} = this.props 
        return(
            <div className="location-list">
                {
                cities.map(city => 
                    <WeatherLocation 
                    key={city} 
                    city={city} 
                    onWeatherLocationClick={ () => this._handleWeatherLocationClick(city, onSelectedLocation)}>
                    </WeatherLocation>
                )
                }
            </div>
        );
    }
}

LocationList.propTypes = {
    cities: PropTypes.array.isRequired,
    onSelectedLocation: PropTypes.func,
}

export default LocationList;