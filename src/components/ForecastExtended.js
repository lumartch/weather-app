import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './styles.css';
import ForecastItem from './ForecastItem/';

const _renderForcastItemDays = (forecastData) => {
    return forecastData.map(forecast => <ForecastItem 
        key={`${forecast.weekDay}${forecast.hour}`} 
        weekDay={forecast.weekDay} 
        hour={forecast.hour}
        data={forecast.data} />
        )
}

const _renderProgress = () => {
    return <h3>Cargando pronóstico...</h3>
}

export class ForecastExtended extends Component {
    render() {
        return(
            <div>
                <h2 className='forecast-title'>Pronóstico del tiempo para {this.props.city}</h2>
                {
                    this.props.forecastData
                        ? _renderForcastItemDays(this.props.forecastData)
                        : _renderProgress()
                }
            </div>
        )
    }
}

ForecastExtended.propTypes = {
    city: PropTypes.string.isRequired
}

export default ForecastExtended;