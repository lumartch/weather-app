import React from 'react';
import PropTypes from 'prop-types';

import './styles.css';

const WeatherExtraInfo = (props) => {
    const { humidity , wind } = props

    return(
        <div className="weather-extra-info-cont">
            <span className="extra-info-text">{`Humedad: ${humidity} %`}</span>
            <span className="extra-info-text">{`Viento: ${wind}`}</span>
        </div>
    )

}

WeatherExtraInfo.defaultProps = {
    humidity: 100,
    wind: '100 m/s'
} 

WeatherExtraInfo.propTypes = {
    humidity: PropTypes.number.isRequired,
    wind: PropTypes.string.isRequired,
}

export default WeatherExtraInfo;