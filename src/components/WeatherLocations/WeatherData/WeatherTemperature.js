import React from 'react';
import WeatherIcons from 'react-weathericons';
import PropTypes from 'prop-types';

import {
    CLOUD,
    SUNNY,
    RAIN,
    SNOW,
    THUNDER,
    DRIZZLE
}   from '../../constants/weathers'

import './styles.css';

const icons = {
    [CLOUD]: 'cloud',
    [SUNNY]: 'day-sunny',
    [RAIN]: 'rain',
    [SNOW]: 'snow',
    [THUNDER]: 'day-thunderstorm',
    [DRIZZLE]: 'day-showers'
};

const getWeatherIcon = weatherState => {
    const icon = weatherState = icons[weatherState];
    const sizeIcon = "4x"
    return (
        <WeatherIcons 
            className="wicon"
            name={icon} 
            size={sizeIcon}>
        </WeatherIcons>
    )
}

const WeatherTemperature = (props) => {
    const { temperature, weatherState } = props
    return(
        <div className="weather-temperature-cont">
            {
                getWeatherIcon(weatherState)
            }
            <span className="temperature">{`${temperature}`}</span> 
            <span className="temperature-type">{` C°`}</span>
        </div>
    )
}

WeatherTemperature.defaultProps = {
    weatherState: 'sunny',
    temperature: 100
}

WeatherTemperature.propTypes = {
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string.isRequired
}

export default WeatherTemperature;