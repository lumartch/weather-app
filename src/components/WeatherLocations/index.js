import React, {Component} from 'react';
import Location from './Location';
import WeatherData from './WeatherData';
import TransformWeather from '../services/TransformWeather';
import './styles.css';
import { CircularProgress } from '@material-ui/core';
import PropTypes from 'prop-types';
import getUrlWeatherByCity from '../services/getUrlWeatherByCity'

class WeatherLocation extends Component{
    constructor(props){
        super(props)
        const { city, onWeatherLocationClick } = props;
        this.state = {
            city: city,
            data: null,
            onWeatherLocationClick: onWeatherLocationClick
        }
    }

    componentDidMount(){
        this._handleUpdateClick();
    }

    _handleUpdateClick = () => {
        fetch(getUrlWeatherByCity(this.state.city)).then( resolve => {
            return resolve.json();
        }).then(data => {
            this.setState({
                data: TransformWeather(data)
            });
        })
    }

    render(){
        const {city, data} = this.state
        return(
            <div className="weather-location-cont" onClick={this.state.onWeatherLocationClick}>
                <Location city={city}></Location>
                {
                data 
                    ? <WeatherData data={data}></WeatherData>
                    : <CircularProgress></CircularProgress>
                }
            </div>
        )
    }
}

WeatherLocation.propTypes = {
    city: PropTypes.string.isRequired,
    onWeatherLocationClick: PropTypes.func,
}

export default WeatherLocation;