import React, {Component} from 'react'
import LocationList from '../components/LocationList';
import PropTypes from 'prop-types';
//Redux
import { connect } from 'react-redux';
import { setSelectedCity } from '../actions'

class LocationListContainer extends Component {
    _handleSelectedLocation = (city) => {
        this.props.setCity(city)
    }

    render(){
        return(
                <LocationList cities={this.props.cities} 
                    onSelectedLocation={this._handleSelectedLocation}></LocationList>
        )
    }
}

LocationListContainer.propTypes = {
    setCity: PropTypes.func.isRequired,
    cities: PropTypes.array.isRequired,
}
  
const mapDispatchToProps = dispatch => ({
    setCity: payload => dispatch(setSelectedCity(payload))
})

export default connect(null, mapDispatchToProps)(LocationListContainer)
//export default LocationListContainer